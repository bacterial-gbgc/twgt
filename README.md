# TWGT

TWGT is a pipeline dedicated to the analysis of genome-wide transformation experiment, as performed by Bubendorfer et al, 2015 or Mell et al, 2014.
It goes from raw reads to tables of positions mapped to donor and recipient genomes.

## Installation

This program is written in Perl v5.24.
As most of bioinformatics software does, it has some dependencies on other softwares:

- [Statistics::Descriptive perl module](https://metacpan.org/pod/Statistics::Descriptive), version >= 3.0702
- [Bio::SearchIO perl module](https://metacpan.org/pod/Bio::SearchIO) module, version >= 1.7.5
- [bcftools samtools, htslib](http://www.htslib.org/), version >= 1.9
- [bwa mem](http://bio-bwa.sourceforge.net/), version >= 0.7.17-r1188
- [MindTheGap](https://github.com/GATB/MindTheGap), version >= 2.2.0
- [seqtk](https://github.com/lh3/seqtk), version >= 1.3-r106
- [wgsim](https://github.com/lh3/wgsim), version >= 1.9
- [pigz](http://zlib.net/pigz/), version >= 2.4
- [blastn](https://www.ncbi.nlm.nih.gov/books/NBK52637/table/blast_setup_pc.T.programs_and_utilities/), version >= 2.8.1
- [mummer & nucmer](https://mummer4.github.io/), version >= 4.0.0beta2
- GNU Coreutils (grep, cut)

Please note that I intend to make this software available via bioconda; until then these software must be installed manually.
(Although you should be mostly lucky in that these software aren’t the toughest to install, with the exception of Bio::SearchIO.)

``` shell
git clone https://gitlab.com/bacterial-gbgc/twgt
cd twgt
# install to /usr/local/bin/twgt
make install
# install to ~/.local/bin/twgt
PREFIX=~/.local/ make install
```

## Usage

```bash
$ twgt --help

Usage: twgt.pl [options] -- a_{1,2}.fq.gz b_{1,2}.fq.gz ...

Options:

  -i,--reads X_1.fq.gz X_2.fq.gz Sequencing reads of sample
  -l,--list fname.txt            File listing paired end reads
* -r,--recipient rec.fa          Recipient assembled genome
* -d,--donor don.fa              Donor assembled genome

  -D,--donor-reads dR1 dR2       Sequencing reads of donor
  -R,--recipient-reads rR1 rR2   Sequencing reads of recipient
  -s,--read-size 151             Size of sequencing reads
  -C,--coverage 50               Target coverage of reference genomes
  -S,--no-subsample              Disable subsampling to 50x coverage
  -o,--outdir                    Output directory

  -c,--cpus 4                    Number of allocated cpus
  -v,--verbose,--no-verbose      Toggle verbose mode on/off
  -n,--dry-run,--no-dry-run      Toggle dry-run mode on/off
  -h,--help                      Display this message.
```

### Examples

Map reads of samples `A`, `B` and `C` to recipient genome `recipient.fa` and donor genome `donor.fa`:

``` shell
twgt --recipient recipient.fa --donor donor.fa --outdir outputs -- {A,B,C}_{1,2}.fq.gz
```

Map reads of samples `A`, `B` and `C` to recipient genome `recipient.fa` and donor genome `donor.fa`; you also happen to have control reads (as is recommended for genotyping control purposes) for donor and recipient strain:

``` shell
twgt \
   --recipient recipient.fa \
   --donor donor.fa \
   --donor-reads donor_{1,2}.fq.gz \
   --recip-reads recip_{1,2}.fq.gz \
   -- {A,B,C}_{1,2}.fq.gz
```

You have high coverage data; however too high a coverage can be quite a burden for genotyping variants. You want to reduce coverage down to a threshold of 30 (50 is default):

``` shell
twgt \
   --recipient recipient.fa \
   --donor donor.fa \
   --coverage 30 \
   -- {A,B,C}_{1,2}.fq.gz
```

Your input reads path are stored in a comma separated value (CSV) file, use that:

``` shell
twgt --recipient recipient.fa --donor donor.fa --coverage 30 -l list__of_reads.csv_
```

### Outputs

``` shell
outdir
├── bam
│   ├── donor.bam [...]   # all samples mapped to donor
│   ├── donor_a.bam       # sample a mapped to donor
│   ├── donor_b.bam       # sample b mapped to donor
│   ├── donor_donor.bam   # control of donor reads mapped to donor
│   ├── donor_recip.bam   # control of recipient reads mapped to donor
│   ├── recip.bam [...]   # all samples mapped to recipient
│   ├── recip_a.bam       # sample a mapped to recipient
│   ├── recip_b.bam       # sample b mapped to recipient
│   ├── recip_donor.bam   # donor mapped to recipient
│   └── recip_recip.bam   # recipient mapped to recipient
├── bcf
│   ├── donor-targets.vcf # list of recipient/donor variants
│   ├── donor.bcf.gz      # variants of all samples with reference to donor
│   ├── donor.bcf.gz.csi  # bcf index for fast querying
│   ├── recip-targets.vcf # list of donor/recipient variants
│   ├── recip.bcf.gz      # variants of all samples with reference to recipient
│   ├── recip.bcf.gz.csi  # bcf index for fast querying
│   └── recip_donor.bedpe # paired-end BED file matching donor and recipient
├── fq
│   ├── a_1.sub.fq.gz     # reads of sample A subsampled to target coverage
│   ├── a_2.sub.fq.gz
│   ├── b_1.sub.fq.gz     # reads of sample B subsampled to target coverage
│   ├── b_2.sub.fq.gz
│   ├── donor_1.sub.fq.gz
│   ├── donor_2.sub.fq.gz
│   ├── recip_1.sub.fq.gz
│   └── recip_2.sub.fq.gz
├── insertions
│   ├── a.fasta           # insertions of donor segment into recipient for sample a
│   ├── a.vcf             # idem as vcf
│   ├── b.fasta           # insertions of donor segment into recipient for sample b
│   └── b.vcf             # idem as vcf
├── ref
│   ├── donor.fa          # donor reference sequence
│   ├── donor.fa.[...]    # index file for bwa mapping
│   ├── recip.fa          # recipient reference sequence
│   ├── recip.fa.[...]    # index file for bwa mapping
├── stats
│   ├── a.fq.stats        # stats of read file (read size, n reads)
│   ├── b.fq.stats
│   ├── donor.fq.stats
│   └── recip.fq.stats
├── tracts
│   ├── a.bcf.gz          # conversion tracts identified
│   ├── a.bcf.gz.csi
│   ├── b.bcf.gz
│   ├── b.bcf.gz.csi
│   ├── donor_a.bed
│   ├── donor_b.bed
│   ├── recip_a.bed
│   ├── recip_b.bed
│   ├── samples.bcf.gz    # all conversion tracts identified
│   └── samples.bcf.gz.csi
├── twgt.log              # detailed log
└── twgt.sh               # log of command ran
```

# Pipeline

TODO

# References

If you use this software, please also cite the following works:
- MindTheGap: integrated detection and assembly of short and long insertions. Guillaume Rizk, Anaïs Gouin, Rayan Chikhi and Claire Lemaitre. Bioinformatics 2014 30(24):3451-3457. http://bioinformatics.oxfordjournals.org/content/30/24/3451
- bwa mem: Li H. and Durbin R. (2009) Fast and accurate short read alignment with Burrows-Wheeler Transform. Bioinformatics, 25:1754-60. [PMID: 19451168]
- samtools, bcftools, htslib: Li H, A statistical framework for SNP calling, mutation discovery, association mapping and population genetical parameter estimation from sequencing data, Bioinformatics (2011) 27(21) 2987-93.

## References mentioned in this file

- Bubendorfer, Sebastian, Juliane Krebes, Ines Yang, Elias Hage, Thomas F. Schulz, Christelle Bahlawane, Xavier Didelot, and Sebastian Suerbaum. “Genome-Wide Analysis of Chromosomal Import Patterns after Natural Transformation of Helicobacter Pylori.” Nature Communications 7 (June 22, 2016): 11995. https://doi.org/10.1038/ncomms11995.
- Mell, Joshua Chang, Jae Yun Lee, Marlo Firme, Sunita Sinha, and Rosemary J. Redfield. “Extensive Cotransformation of Natural Variation into Chromosomes of Naturally Competent Haemophilus Influenzae.” G3: Genes, Genomes, Genetics 4, no. 4 (April 1, 2014): 717–31. https://doi.org/10.1534/g3.113.009597.

# Licence

[![](https://www.gnu.org/graphics/gplv3-88x31.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)
