#!/usr/bin/env perl

use strict;
use warnings;
use v5.24;

use Cwd qw(abs_path);
use Data::Dumper;
use File::Basename;
use File::Copy qw(copy);
use File::Path qw(make_path);
use File::Temp;
use FindBin qw($Script);
use Getopt::Long qw(GetOptions);
use List::Util qw(max sum notall pairs reduce);
use POSIX qw(strftime floor ceil);
use Statistics::Descriptive;
use Time::HiRes qw(time);

use constant DEBUG => $ENV{'DEBUG'};
use constant EXE   => File::Basename::basename(__FILE__);
my $start_time = time();

my (
    $arg_verbose, $arg_recip,  $arg_donor, $arg_cpus,
    $arg_dryrun,  $arg_outdir, $arg_rsize
);
$arg_verbose = 1;
my $arg_help = 0;
my $arg_file_list;
my @arg_reads;
my @arg_donor_reads;
my @arg_recip_reads;
my $arg_nosubsample = 0;
my $arg_coverage    = 50;
my $arg_citations;

usage(1) unless @ARGV;

# FIXME: something is wrong with the parsing of donor and recipient reads.
# I think it has to do with the underlying logic.
# Also no subsampling is performed.

GetOptions(
    'i|reads=s{2}'           => \@arg_reads,
    'l|list=s'               => \$arg_file_list,
    'D|donor-reads=s{2}'     => \@arg_donor_reads,
    'R|recipient-reads=s{2}' => \@arg_recip_reads,
    's|read-size=i'          => \$arg_rsize,
    'r|recipient=s'          => \$arg_recip,
    'd|donor=s'              => \$arg_donor,
    'o|outdir=s'             => \$arg_outdir,
    'c|cpus=i'               => \$arg_cpus,
    "C|coverage=i"           => \$arg_coverage,
    'h|help+'                => \$arg_help,
    'S|no-subsample'         => \$arg_nosubsample,
    'v|verbose!'             => \$arg_verbose,
    'n|dry-run!'             => \$arg_dryrun,
    "cite"                   => \$arg_citations,
) or usage(1);

citations() and exit(0) if $arg_citations;
usage(1) if $arg_help;
$arg_cpus   ||= 4;
$arg_outdir ||= get_prefix( $arg_reads[0] );
$arg_outdir =~ s/\/$//;

my $log_cmd = "$arg_outdir/twgt.sh";
my $log_log = "$arg_outdir/twgt.log";

# log files
-f $log_cmd and unlink $log_cmd;
-f $log_log and unlink $log_log;

if ($arg_file_list) {
    @arg_reads = parse_file_list($arg_file_list);
}
else {
    @arg_reads = @ARGV unless @arg_reads;
}

  err ("Genome '$arg_donor' does not exists") unless -f $arg_donor;
  err ("Genome '$arg_recip' does not exists") unless -f $arg_recip;
  err ("Reads not specified")                 unless @arg_reads;
  err ("At least one file in '@arg_reads' does not exists")
  if notall_exists(@arg_reads);

mkp("$arg_outdir/ref");
mkp("$arg_outdir/fq");
mkp("$arg_outdir/stats");
mkp("$arg_outdir/bam");
mkp("$arg_outdir/bcf");
mkp("$arg_outdir/insertions");
mkp("$arg_outdir/tracts");
mkp("$arg_outdir/.log");
mkp("$arg_outdir/.cache");

greetings();
echo_info() if $arg_verbose;

msg("= Creating directory structure:");
msg("'$arg_outdir/ref' contains reference genomes");
msg("'$arg_outdir/fq' contains sequencing reads");
msg("'$arg_outdir/stats' contains summary statistics");
msg("'$arg_outdir/bam' contains mapped reads");

msg("Copying recipient and donor genome to ref directory");
-f "$arg_outdir/ref/recip.fa"
  or $arg_dryrun
  or run_cmd("cp $arg_recip $arg_outdir/ref/recip.fa");
-f "$arg_outdir/ref/donor.fa"
  or $arg_dryrun
  or run_cmd("cp $arg_donor $arg_outdir/ref/donor.fa");
$arg_recip = "$arg_outdir/ref/recip.fa";
$arg_donor = "$arg_outdir/ref/donor.fa";
rule();

msg("= Genome index:");
msg("Checking index of '$arg_recip'.");
index_genome($arg_recip);
msg("Checking index of '$arg_donor'.");
index_genome($arg_donor);
rule();

my @donor_reads;
my @recip_reads;
unless ( @arg_donor_reads && @arg_recip_reads ) {
    msg("= Simulating reference reads:");
    msg("Estimating read size from R1");
    $arg_rsize ||= estimate_rsize( $arg_reads[0] );
    msg("Read size was set to '$arg_rsize'");

    msg("Computing number of reads to get to ${arg_coverage}x coverage");
    my @needed_reads = map { estimate_nreads($_) } ( $arg_recip, $arg_donor );
    my $arg_nreads   = max @needed_reads;
    msg("Need $arg_nreads to get to ${arg_coverage}x coverage for both genomes."
    );

    unless (@arg_recip_reads) {
        msg("Simulating recipient genome reads");
        @recip_reads = simulate_reads( "recip", $arg_recip, $arg_nreads );
    }
    else {
        @recip_reads = @arg_recip_reads;
    }

    unless (@arg_donor_reads) {
        msg("Simulating donor genome reads");
        @donor_reads = simulate_reads( "donor", $arg_donor, $arg_nreads );
    }
    else {
        @donor_reads = @arg_donor_reads;
    }
    rule();
}
else {
    @donor_reads = @arg_donor_reads;
    @recip_reads = @arg_recip_reads;
}

my $dpref = get_prefix(@donor_reads);
my $rpref = get_prefix(@recip_reads);

unless ($arg_nosubsample) {
    msg("= Subsampling reference reads:");
    msg("Subsampling donor reads R1 and R2 to ${arg_coverage}x coverage");
    @donor_reads = subsample_reads( $arg_recip, $arg_donor, @donor_reads );
    msg("Subsampling recipient reads R1 and R2 to ${arg_coverage}x coverage");
    @recip_reads = subsample_reads( $arg_recip, $arg_donor, @recip_reads );
    rule();
}

my @recip_mapped_reads;
my @donor_mapped_reads;
msg("= Mapping reference reads:");
msg(    "Mapping donor reads to recipient and donor genomes "
      . "'$arg_recip' and '$arg_donor'" );
my $donor2recip = map_reads( "donor_recip", $arg_recip, @donor_reads );
my $donor2donor = map_reads( "donor_donor", $arg_donor, @donor_reads );
msg(    "Mapping recipient reads to recipient and donor genomes "
      . "'$arg_recip' and '$arg_donor'" );
my $recip2donor = map_reads( "recip_donor", $arg_donor, @recip_reads );
my $recip2recip = map_reads( "recip_recip", $arg_recip, @recip_reads );
push @recip_mapped_reads, ( $donor2recip, $recip2recip );
push @donor_mapped_reads, ( $donor2donor, $recip2donor );
rule();

my @sample_names;
for my $readspe ( pairs @arg_reads ) {
    my @reads = @$readspe;
    my $pref  = get_prefix( $reads[0] );
    push @sample_names, $pref;

    msg("= Sample $pref");
    msg("== Subsampling reads @reads to ${arg_coverage}x coverage");
    @reads = subsample_reads( $arg_recip, $arg_donor, @reads );
    msg("Mapping R1 and R2 to recipient genome '$arg_recip'");
    my $reads2recip = map_reads( "recip_$pref", $arg_recip, @reads );
    push @recip_mapped_reads, $reads2recip;

    msg("== Mapping R1 and R2 to donor genome '$arg_donor'");
    my $reads2donor = map_reads( "donor_$pref", $arg_donor, @reads );
    push @donor_mapped_reads, $reads2donor;
    rule();

    msg("== Finding insertions in recipient");
    find_insertions( $pref, $arg_recip, @reads );
    rule();
}

msg("Merging reads mapped to recipient '$arg_recip'");
my $bam2recip = merge_bam( "recip", $arg_recip, @recip_mapped_reads );
msg("Merging reads mapped to donor '$arg_donor'");
my $bam2donor = merge_bam( "donor", $arg_donor, @donor_mapped_reads );

msg("Creating target file for variant calling on donor ...");
my $donor_target =
  target_variants( "donor-targets", $bam2donor, $arg_donor, $rpref );
msg("Creating target file for variant calling on recipient ...");
my $recip_target =
  target_variants( "recip-targets", $bam2recip, $arg_recip, $dpref );

msg("Getting per snp correspondance between donor and recipient ...");
my $snp_table = nucmer_donor_recipient( $arg_recip, $arg_donor );

msg("Pileup mapped reads of $bam2recip on reference $arg_recip");
my $bcf2recip = call_variants_recip( $bam2recip, $recip_target );
msg("Pileup mapped reads of $bam2donor on reference $arg_donor");
my $bcf2donor = call_variants_donor( $bam2donor, $donor_target );

msg("========================================================================");
msg("= Conversion Tracts Calling                                            =");
msg("========================================================================");
my @sample_bcfs;
foreach my $sm (@sample_names) {
    msg("== Sample $sm");
    my $rtable = conversion_tracts_recip($sm);
    my $dtable = conversion_tracts_donor($sm);

    my $smbcf =
      tract_confirmation( $sm, $rtable, $bcf2recip, $bcf2donor, $snp_table );
    push @sample_bcfs, $smbcf;
    rule();
}

msg("Merging all donor identified conversion tracts in one bcf file.");
my $merged_bcf = bcf_merge( "$arg_outdir/tracts/samples.bcf.gz", @sample_bcfs );

rule();
farewell();

# __END__ #

# ===============================================================================

=head2 Insertions Calling

=cut

# ===============================================================================

# TODO should document insertions calling and use bioperl to parse the
# blast output.

sub blastn {
    my ( $qry, $sbjct ) = @_;
    my @out = get_cmd("blastn -db $sbjct -query $qry -outfmt '6 std qcovus'");
    dbg("#chr\tqpos\tqstart\tqend\tschr\tsstart\tsend\tidentity\tqcov");
    foreach (@out) {
        chomp;
        my (
            $query,     $subject, $identity, $length,  $mismatches,
            $gap_opens, $q_start, $q_end,    $s_start, $s_end,
            $evalue,    $score,   $qcov
        ) = split /\t/, $_;
        next if $evalue > 0.01;      # eliminate spurious hits
        next if $identity < 80.0;    # remove if identity is too low.
        next if $qcov < 99;          # can't come from donor if not all in it.
        my $qpos = insertions_get_pos($query);
        say tab(
            insertions_get_chr($query),
            $qpos,    $q_start, $q_end,    $subject,
            $s_start, $s_end,   $identity, $qcov
        );
    }
}

=head3 insertions_get_pos

Returns the position stored by MindTheGap in the fasta header of
sequence.

=cut

sub insertions_get_pos {
    my ($line) = @_;
    $line =~ s/^.*pos_(\d*)_.*$/$1/;
    return $line;
}

=head3 insertions_get_chr

Returns the name of contig where an insertion was detected.
Stored in the fasta header by MindTheGap.

=cut

sub insertions_get_chr {
    my ($line) = @_;
    $line =~ s/^bkpt\d*_(.*)_pos.*/$1/;
    return $line;
}

sub find_insertions {
    my ( $pref, $gnm, $r1, $r2 ) = @_;
    msg("Find insertions in recipient for sample $pref ...");
    my $dir      = newdir();
    my $tmp      = newdir();
    my $oname    = "$arg_outdir/insertions/$pref.fasta";
    my $tmponame = "$dir/$pref";
    my $mtglog   = "$arg_outdir/.log/mtg.$pref.log";
    my $cmd =
        qq(MindTheGap find -nb-cores $arg_cpus -out-tmp $tmp -insert-only)
      . qq( -in $r1,$r2 -ref $gnm -out $tmponame 2> $mtglog > /dev/null && )
      . qq(MindTheGap fill -nb-cores $arg_cpus -graph $dir/$pref.h5 )
      . qq( -bkpt $dir/$pref.breakpoints -out $tmponame )
      . qq( 2>> $mtglog > /dev/null )
      . qq( && mv "$tmponame.insertions.fasta" "$arg_outdir/insertions/$pref.fasta" )
      . qq( && mv "$tmponame.insertions.vcf"   "$arg_outdir/insertions/$pref.vcf" );

    -f $oname or force_cmd($cmd, 34304);
    return $oname;
}

# ===============================================================================

=head2 Conversion Tract Calling

=cut

# ================================================================================

=head3 bcf_merge

  usage: my $merged_bcf = bcf_merge($outname, $bcfs);

=cut

sub bcf_merge {
    my ( $outname, @bcfs ) = @_;
    -f $outname
      or run_cmd( "bcftools merge -Ob -o $outname @bcfs "
          . "&& bcftools index -f $outname" );
}

=head3 tract_confirmation

  usage: tract confirmation($tract_table, $recip_bcf, $donor_bcf, $snp_table);
   args:
       - $tract_table: the list of identified conversion tracts
       - $recip_bcf: the variant calling performed by mapping on recipient
       - $donor_bcf:                                             donor
       - $snp_table: the paired-end bed file which repertoriates the
                     position of snps betweeen donor and recipient.

For each tracts of sample recensed in I<$tract_table>, find if the
genotyping we get by getting SNPs on the recipient matches the
genotype we get by getting SNPs from the donor.

=cut

sub tract_confirmation {
    my ( $sample, $tract_table, $recip_bcf, $donor_bcf, $snp_table ) = @_;
    dbg($tract_table);
    my $samples      = "$rpref,$dpref,$sample";
    my $filter_query = filter_donor_query();
    my $tmpdir       = newdir();
    my $ovcf         = "$tmpdir/$sample.vcf";
    my $obcf         = "$arg_outdir/tracts/$sample.bcf.gz";
    unless ( -f $obcf ) {
        open OUTPUT, '>>', $ovcf;
        my %header = bcf_add_format(
            "$donor_bcf -s '$rpref,$dpref,$sample'", "TN",
            "1",                                     "Integer",
            "Conversion Tract Identifier"
        );
        bcf_write_header( $ovcf, %header );
        open my $tract_fh, "<", $tract_table;
        while ( my $t = <$tract_fh> ) {
            chomp $t;
            dbg($t);
            my $tn = bedline_get_tn($t);

            # contains a bedpe file that maps for each snps in tract of
            # recipient coordinates recensed in $t its coordinates on
            # donor genome.
            my $f1 = tempfile();

            # contains for each snp of recipient tract $t the
            # corresponding snp on donor.
            my $f2 = tempfile();

            my %refalt = bcf_get_tract_refalt( $recip_bcf, $samples, $t );
            say STDERR Dumper( \%refalt );

            # get snps of tract $t, and returns its coordinates on donor
            my $snp_correspondance = bcf_fetch( $recip_bcf, $samples, $t )
              . qq(| bedtools intersect -a $snp_table -b - > $f1);
            run_cmd( $snp_correspondance, 1 );
            my %snph =
              pair_snp($f1);    # correspondance between donor and recip snp

            # get potential conversions for this sample on donor, and
            # intersect it with the corresponding coordinates
            my $donor_fetch =
                qq(bcftools view -s '$rpref,$dpref,$sample' $donor_bcf)
              . qq(| bcftools filter -i '$filter_query');
            my $donor_snp =
                qq(bedtools intersect -a <($donor_fetch))
              . qq( -b <(cut -f4- $f1) > $f2);
            run_cmd( $donor_snp, 1 );

            open INPUT, '<', $f2;
            my $tract_position = 0;
            while ( my $dsnp = <INPUT> ) {
                chomp $dsnp;
                $tract_position += 1;
                my $_key = bcfline_get_pairkey( $dsnp, %snph )
                  ;    # get coordinates of recip for coordinates of donor
                my $donor_altref = bcfline_get_altref($dsnp)
                  ;    # get altref (corresponding to ref->alt for recip)
                my $recip_refalt = $refalt{$_key};
                dbg("$recip_refalt -> $donor_altref");
                if ( $recip_refalt eq $donor_altref ) {
                    say OUTPUT tab( bcf_set_format_tn( $dsnp, $tn ) )
                      ;    # write line to output vcf
                }
                else {
                    dbg(
"Genotype doesn't match for position $tract_position of tract $tn:\t"
                          . "$recip_refalt -> $donor_altref" );
                }
            }
            close INPUT;
        }
        close OUTPUT;
        vcf2bcf( "$ovcf -s '$sample'", $obcf ) and unlink $ovcf;
    }
    return $obcf;
}

=head3 bcfline_get_pairkey

  usage: my $key = bcfline_get_pairkey($bcfline);

Return a key composed of the contig name and the position on this
contig, that is used to query a table constructed using pair_snp.

=cut

sub bcfline_get_pairkey {
    my ( $bcfline, %snph ) = @_;
    my @l     = split /\t/, $bcfline;
    my $query = "$l[0]$l[1]";
    return $snph{$query};
}

=head3 pair_snp

  usage: my %ht = pair_snp($bedpe);

=cut

sub pair_snp {
    my ($bedpe) = @_;
    my %out;
    open my $fh, '<', $bedpe;
    while (<$fh>) {
        chomp;
        my @rec  = split /\t/;
        my $_key = "$rec[3]$rec[5]";
        my $_val = "$rec[0]$rec[2]";
        $out{$_key} = $_val;
    }
    close $fh;
    return %out;
}

=head3 bcfline_get_altref

  usage: my $donor_altref = bcfline_get_altref($bcfline);

=cut

sub bcfline_get_altref {
    my ($bcfline) = @_;
    my @rec = split /\t/, $bcfline;
    return "$rec[4]>$rec[3]";
}

=head3 bcf_get_tract_refalt

  usage: my @refalt = bcf_get_tract_refalt($bcf, $sample, $tract);

=cut

sub bcf_get_tract_refalt {
    my ( $bcf, $sample, $tract ) = @_;
    my $cmd = bcf_fetch( $bcf, $sample, $tract );
    my %out;
    open my $infh, "$cmd |";
    while (<$infh>) {
        chomp;
        next if /^#/;
        my @rec  = split /\t/;
        my $_key = "$rec[0]$rec[1]";
        my $_val = "$rec[3]>$rec[4]";
        $out{$_key} = $_val;
    }
    close $infh;
    return %out;
}

=head3 bcf_set_format_tn

  usage: bcf_set_format_tn($bcf_line, $tn);

=cut

sub bcf_set_format_tn {
    my ( $bcf_line, $tn ) = @_;
    my @rec = split /\t/, $bcf_line;
    $rec[8]  .= ":TN";
    $rec[9]  .= ":.";
    $rec[10] .= ":.";
    $rec[11] .= ":$tn";
    return @rec;
}

=head3 bcf_write_header

  usage: bcf_write_header($fh, %header);

Write the bcf header %header to file handle $fh;

=cut

sub bcf_write_header {
    my ( $fname, %header ) = @_;
    open my $fh, ">>", $fname;
    map { say $fh $_ } @{ $header{mdat} };
    map { say $fh $_ } @{ $header{head} };
    close $fh;
}

=head3 bcf_add_format

  usage: bcf_add_format($bcf, $id, $number, $type, $description);

=cut

sub bcf_add_format {
    my ( $bcf, $id, $number, $type, $description ) = @_;
    my %header = bcf_header($bcf);
    my $format =
qq(##FORMAT=<ID=$id,Number=$number,Type=$type,Description="$description">);
    push @{ $header{mdat} }, $format;
    return %header;
}

=head3 bcf_header

  usage: bcf_header($bcf);

Get the header of bcf file $bcf, and return it as a hash, with
metadata stored in $hash{mdat} and header line per se stored in
$hash{head}.

=cut

sub bcf_header {
    my ($bcf) = @_;
    open my $fh, "bcftools view -h $bcf |";
    my %header;
    while (<$fh>) {
        chomp;
        push @{ $header{mdat} }, $_ if /^##/;
        push @{ $header{head} }, $_ if /^#CHROM/;
    }
    close $fh;
    return %header;
}

=head3 bedline_get_tn

  usage: bedline_get_tn($bedline);

Return the tract number stored in metadata column of bed file line.

=cut

sub bedline_get_tn {
    my ($bedline) = @_;
    my @line = split /\t/, $bedline;
    return $line[3];
}

=head3 bcf_fetch

  usage: my $fetch_cmd = bcf_fetch($bcf, $samples, $line);
Fetch from indexed bcf file $bcf the samples (comma separated)
$samples, using coordinates (chr:a-b) recensed in $line (tab
separated), usually a bed file.

=cut

sub bcf_fetch {
    my ( $bcf, $samples, $line ) = @_;
    my @query = split /\t/, $line;
    my $cmd   = "bcftools view -r '$query[0]:$query[1]-$query[2]'"
      . " -s '$samples' $bcf";
    return $cmd;
}

sub nucmer_donor_recipient {
    my ( $recip, $donor ) = @_;
    my $snp_bedpe = "$arg_outdir/bcf/${rpref}_${dpref}.bedpe";
    my $tmpdir    = newdir();
    my ( $out_fh, $in_fh );
    unless ( -f $snp_bedpe ) {
        my $delta         = "$tmpdir/out.delta";
        my $nucmer_snps   = "$tmpdir/out.snps";
        my $nucmer_cmd    = "nucmer --delta $delta $recip $donor";
        my $show_snps_cmd = "show-snps -H -C -T -r -I $delta > $nucmer_snps";

        # -H removes header
        # -C keep only non-ambiguous mapping
        # -T use tab delimited format
        # -r sort by reference ids and positions
        # -I exclude indels.
        run_cmd($nucmer_cmd) and run_cmd($show_snps_cmd);
        open( $in_fh,  "<$nucmer_snps" );
        open( $out_fh, ">>$snp_bedpe" ) or die("Can't write to $snp_bedpe .");
        while ( my $line = <$in_fh> ) {
            chomp $line;
            my @snps = split /\t/, $line;
            say $out_fh tab(
                $snps[8], $snps[0] - 1,
                $snps[0], $snps[9], $snps[3] - 1,
                $snps[3]
            );
        }
        close $out_fh;
        close $in_fh;
    }
    return $snp_bedpe;
}

sub conversion_tracts_donor {
    my ($sm)         = @_;
    my $tfile        = "$arg_outdir/tracts/donor_$sm.bed";
    my $donor_tracts = $tfile;
    msg("=== On donor");
    unless ( -f $tfile ) {
        msg("Estimating distance between conversion tracts");
        my $donor_dist =
          get_distance_threshold( $sm, $bcf2donor, \&query_donor_bcf );
        dbg($donor_dist);
        msg("Calling conversion tracts location");
        $donor_tracts = call_conversion_tracts( $sm, $bcf2donor, $donor_dist,
            \&query_donor_bcf, $tfile );
    }
    return $tfile;
}

=head3 conversion_tracts_recip

  usage: my $tracts = conversion_tracts_recip($sample_name);

Call conversion tract on recipient

Returns the path to a bed file containing conversion tracts on
recipient.

=cut

sub conversion_tracts_recip {
    my ($sm)         = @_;
    my $tfile        = "$arg_outdir/tracts/recip_$sm.bed";
    my $recip_tracts = $tfile;
    msg("=== On recipient");
    unless ( -f $tfile ) {
        msg("Estimating distance between conversion tracts");
        my $recip_dist =
          get_distance_threshold( $sm, $bcf2recip, \&query_recip_bcf );
        dbg($recip_dist);
        msg("Calling conversion tracts location");
        $recip_tracts = call_conversion_tracts( $sm, $bcf2recip, $recip_dist,
            \&query_recip_bcf, $tfile );
    }
    return $tfile;
}

sub samtools_region {
    my ($in)   = @_;
    my (@line) = split /\t/, $in;
    return "$line[1]:$line[2]-$line[3]";
}

=head3 call_conversion_tracts

  usage: call_conversion_tracts($sample, $bcf, $distance_threshold,
                                $query_function, $tracts_file);

For sample $sample whose variants are recensed in $bcf file, query the
bcf file using $query_function to write to $tracts_file the location
of conversion tracts. Two markers are attributed to two distinct
conversion tracts if they are more than $distance_threshold bp appart.

$tracts_file has bed output format:
chr	pos1	pos2	tract_name

Returns the path to $tracts_file.

=cut

sub call_conversion_tracts {
    my ( $sm, $bcf, $threshold, $query, $tfile ) = @_;
    my @convs = $query->( $sm, $bcf );
    my ( $left,     $right,   $ntract,   $i );
    my ( $prev_chr, $cur_chr, $prev_pos, $cur_pos );
    $i      = 0;
    $ntract = 1;

    unless ( -f $tfile ) {
        open my $fh, '>', $tfile or die("Could not open $tfile");
        my $inc_tracts = sub {
            my $bedleft = $left - 1;
            my $line    = "$prev_chr\t$bedleft\t$right\t$ntract";
            dbg($line);
            say $fh $line;
            ++$ntract;
            $left  = $cur_pos;
            $right = $cur_pos;
        };

        foreach my $rec (@convs) {
            chomp $rec;
            my (@current_rec) = split /\t/, $rec;
            if ( $i == 0 ) {
                dbg("first record");
                $left  = $current_rec[1];
                $right = $current_rec[1];
                ++$i;
                next;
            }

            my (@previous_rec) = split /\t/, $convs[ $i - 1 ];
            ( $prev_chr, $cur_chr ) = ( $previous_rec[0], $current_rec[0] );
            ( $prev_pos, $cur_pos ) = ( $previous_rec[1], $current_rec[1] );
            my $eqchr = $cur_chr eq $prev_chr;
            my $blwth = ( $cur_pos - $prev_pos ) < $threshold;
            $eqchr && $blwth ? $right = $cur_pos : &$inc_tracts;
            ++$i;
        }
        close $fh;
    }
    return $tfile;
}

=head3 get_distance_threshold

  usage: get_distance_threshold($sample, $bcf, $query_function);

Query the bcf file $bcf using the function $query_function (see also
query_donor_bcf and query_recip_bcf) for sample $sample.

Returns the threshold that sets two tracts appart, <ie> the distance
above which two converted markers are said to be in two distinct
conversion tracts.

=cut

sub get_distance_threshold {
    my ( $sm, $bcf, $query ) = @_;
    my @dists = $query->( $sm, $bcf );
    my ( @chr, @pos, @diff );
    foreach (@dists) {
        chomp;
        my ( $chr, $pos ) = split /\t/, $_;
        push @chr, $chr;
        push @pos, $pos;
    }
    for my $i ( 1 .. $#pos ) {
        if ( $chr[$i] eq $chr[ $i - 1 ] ) {
            push @diff, $pos[$i] - $pos[ $i - 1 ];
        }
    }

    #dbg("@diff");
    my $stat = Statistics::Descriptive::Full->new();
    $stat->add_data(@diff);
    return ( $stat->percentile(97.5) );
}

# ================================================================================

=head2 Variant Calling

The variant calling is done in two way (twgt).

First, donor reads are mapped on the recipient to identify potentially
converted sites. Then reads of samples (transformant clones) are
mapped to the same recipient, and variant calling is performed only at
these previously identified target sites.

Second, the inverse approach is performed: recipient reads are mapped
to the donor assembled genome, and markers identified. Then converted
markers are identified by mapping sample reads to the donor, and
filtering to keep only these sites where the genotype is the same as
the donor, <ie> consensus sites.

=cut

# ================================================================================

=head3 query_donor_bcf

Returns the output of querying the bcf file for donor, ie sites that
are consensus with the reference sequence, here the donor.

=cut

sub query_donor_bcf {
    my ( $sample, $bcf ) = @_;
    my $query_sample = "$rpref,$dpref,$sample";
    my $filter_query = filter_donor_query();
    my $cmd =
        "bcftools view -s $query_sample $bcf "
      . qq(| bcftools filter -i '$filter_query' )
      . qq(| bcftools view -H );
    return get_cmd($cmd);
}

sub filter_donor_query {

    # NOTE: sample should be sorted accordingly: recip,donor,sample
    # by upstream command.
    my $depth_thres = $arg_coverage / 2;
    return qq(GT[0]="1" && GT[1]=="0" && GT[2]=="0" && DP>"$depth_thres");
}

=head3 query_recip_bcf

  usage: query_recip_bcf($sample, $bcf)

Returns the output of querying the bcf file for recipient, ie sites
that are consensus with the recipient sequence.

$sample is the name of sample, used to query the bcf file.
$bcf is the path to bcf file.

=cut

sub query_recip_bcf {
    my ( $sample, $bcf ) = @_;
    my $depth_thres  = $arg_coverage / 2;
    my $query_sample = "$rpref,$dpref,$sample";
    my $filter_query =
      qq(GT[0]="0" && GT[1]=="1" && GT[2]=="1" && DP>"$depth_thres");
    my $cmd =
        "bcftools view -s $query_sample $bcf "
      . qq(| bcftools filter -i '$filter_query' )
      . qq(| bcftools view -H )
      . qq(| cut -f1,2);
    return get_cmd($cmd);
}

=head3 call_variants_donor

Call variants with donor genome.

=cut

sub call_variants_donor {
    my ( $ibam, $target ) = @_;
    my $oname = "$arg_outdir/bcf/donor.bcf.gz";
    return call_variants( $oname, $ibam, $arg_donor, $target );
}

=head3 call_variants_recip

Call variants with recipient genome.

=cut

sub call_variants_recip {
    my ( $ibam, $target ) = @_;
    my $oname = "$arg_outdir/bcf/recip.bcf.gz";
    return call_variants( $oname, $ibam, $arg_recip, $target );
}

=head3 call_variants

  usage: call_variants($output_path, $input_bam, $genome, $target);

Call variants for all samples in $input_bam with reference genome
$genome, looking only at sites referenced in $target.
Returns the path to the output.

=cut

sub call_variants {
    my ( $oname, $ibam, $gnm, $target ) = @_;
    my $cmd =
        qq(bcftools mpileup -f $gnm $ibam )
      . qq(| bcftools call -c --ploidy 1 -V indels -T $target -v )
      . qq(| bcftools annotate -x '^INFO/MQ,INFO/DP' -Ob -o $oname );
    -f $oname or run_cmd($cmd);
    -f $oname . ".csi" or run_cmd("bcftools index -f $oname");
    return $oname;
}

=head3 target_variants

  usage: target_variants($output_prefix, $input_bam, $genome, $sample_prefix);

Create a target file for subsequent variant calling.
Returns path to the target file.

This serves as a kind of bootstrapping of variant calling, to speed up
downwards computation. Since we are looking for conversions, most of
genomic sites between donor and recipient are not informative (unless
for checking for de-novo mutations, but this is another story), and
there is no need to check for variants in these regions.

This commands thus run a first involved bcftools calling command, that
will determine each polymorphic sites between donor and recipient, by
extracting from mapped reads all reads from donor, if calling variant
with recipient, or from recipient, if calling variant with donor. It
then pileup reads on reference and call variants.

This <target> file is used by downwards per-sample variant calling, to
look only for variants in these regions.

=cut

sub target_variants {
    my ( $opref, $ibam, $gnm, $spref ) = @_;
    my $oname = "$arg_outdir/bcf/$opref.vcf";
    my $cmd =
        qq(bcftools mpileup -f $gnm -s $spref $ibam )
      . qq(| bcftools call -c --ploidy 1 -V indels -v )
      . qq(| bcftools annotate -x '^INFO/MQ,INFO/DP' -Ov -o $oname );
    -f $oname or run_cmd($cmd);
    return $oname;
}

=head3 vcf2bcf

  usage: my $bcf_file = vcf2bcf($i, $o);

=cut

sub vcf2bcf {
    my ( $i, $o ) = @_;
    -f $o
      or run_cmd(
        "bcftools view $i | bcftools sort -Ob -o $o && bcftools index $o");
    return $o;
}

=head3 bcf2vcf

  usage: bcf2vcf($bcf1,$bcf2,...$bcf_n)

Convert input bcf file to vcf and vcf.gz and index output file.

=cut

sub bcf2vcf {
    foreach my $in (@_) {
        ( my $vcf   = $in ) =~ s/\.bcf\.gz/.vcf/;
        ( my $vcfgz = $in ) =~ s/\.bcf\.gz/.vcf.gz/;
        -f $vcf            or run_cmd("bcftools convert -Ov -o $vcf $in");
        -f $vcfgz          or run_cmd("bcftools convert -Oz -o $vcfgz $in");
        -f $vcfgz . ".csi" or run_cmd("bcftools index -f $vcfgz");
        -f $in . ".csi"    or run_cmd("bcftools index -f $in");
    }
}

=head3 merge_bam

Merge all bam file in input to a single bam file mapped on reference.
Index the output bam file as well.

Returns path to merged bam file.

=cut

sub merge_bam {
    my ( $name, $gnm, @bams ) = @_;
    my $out_bam = "$arg_outdir/bam/$name.bam";
    -f $out_bam
      or run_cmd(
        "samtools merge -@ $arg_cpus -f -O BAM --reference $gnm $out_bam @bams"
      );
    -f $out_bam . ".bai" or run_cmd("samtools index -@ $arg_cpus $out_bam");
    return $out_bam;
}

=head3 map_reads

Map reads to reference genome using bwa mem.
Returns the path to the output bam file.

Reads are sorted and duplicate-marked using samtools.

=cut

sub map_reads {
    my ( $out, $ref, $r1, $r2 ) = @_;
    my $out_bam = "$arg_outdir/bam/$out.bam";
    my $bwa     = bwa_cmd( ( $ref, $r1, $r2 ) );
    my $sortopt = "-@ $arg_cpus --reference $ref";
    my $cmd     = "$bwa"
      . "| samtools view -h -f P -F 4 -b - "
      . "| samtools sort $sortopt -n - "
      . "| samtools fixmate -m - - "
      . "| samtools sort $sortopt - "
      . "| samtools markdup -r -s - - "
      . "> $out_bam";
    run_cmd("$cmd") unless -e $out_bam;
    return $out_bam;
}

=head3 bwa_cmd

Returns the default bwa mem command for read mapping reads R1 and R2
to reference.

Sets read groups and sample ids as well, for downwards querying.

=cut

# TODO: implement logging bwa output to some kind of logging file, so
# that bwa doesn't fill out the whole terminal.

sub bwa_cmd {
    my ( $ref, $r1, $r2 ) = @_;
    my $sid = get_prefix($r1);
    my $rg  = "'\@RG\\tID:$sid\\tSM:$sid\\tPL:ILLUMINA\\tPI:300'";
    my $bwa = "bwa mem -K 100000000 -v 3 -t $arg_cpus -Y -R $rg $ref $r1 $r2";
    return $bwa;
}

# ================================================================================

=head2 Read Subsampling

Most of the time coverage is way too high for our simple SNP calling
needs; this have no point but to increase the computation burden, so
sequencing depth is reduced to match a given coverage of 50x by
default, unless specified explicitely with <--coverage>.

=cut

# ================================================================================

=head3 subsample_reads

  usage: subsample($recip, $donor, $R1, $R2);

Subsample R1 and R2 so as to cover both <$recip> and <$donor> to
<$arg_coverage>x.

No subsampling will be performed in three conditions:
1. --no-subsample was passed as script argument
2. estimated coverage is below target coverage
3. estimated coverage is approximately equal to target coverage.

Returns the path to subsampled R1 and R2.

=cut

sub subsample_reads {
    my ( $recip, $donor, $R1, $R2 ) = @_;
    my $name     = get_prefix($R1);
    my $r_gsize  = read_gsize("$recip.fai");
    my $d_gsize  = read_gsize("$donor.fai");
    my $gsize    = max( $r_gsize, $d_gsize );
    my $total_bp = read_stats($R1);
    my $coverage = $total_bp / $gsize;
    msg("Estimated sequencing depth: ${coverage}x");

    if ($arg_nosubsample) {
        msg("--no-subsample was passed. Skipping subsampling.");
        return ( $R1, $R2 );
    }
    elsif ( $coverage < $arg_coverage ) {
        msg("Coverage is below $arg_coverage. No subsampling performed");
        return ( $R1, $R2 );
    }
    elsif ( round($coverage) eq round($arg_coverage) ) {
        msg("Coverage is already $arg_coverage. No subsampling performed");
        return ( $R1, $R2 );
    }
    else {
        my $factor = sprintf "%.6f", $arg_coverage / $coverage;
        my @out_fq =
          map { "$arg_outdir/fq/$name" . $_ } qw(_1.sub.fq.gz _2.sub.fq.gz);
        msg(
"Subsampling reads by factor $factor to get from ${coverage}x to ${arg_coverage}x"
        );
        seqtk_subsample( $R1, $factor, $out_fq[0] );
        seqtk_subsample( $R2, $factor, $out_fq[1] );
        return @out_fq;
    }
}

sub seqtk_subsample {
    my ( $iread, $factor, $oread ) = @_;
    my $pigzopt = "--fast -c -p $arg_cpus";
    my $cmd =
      "seqtk sample -s100 \Q$iread\E $factor " . "| pigz $pigzopt > $oread";
    run_cmd($cmd) unless -e $oread;
}

sub read_stats {
    my ($R1) = @_;
    if ($arg_dryrun) {
        msg("In dry-run mode so total sequenced base estimated to 1000 kb");
        return 1000000;
    }
    else {
        my $name = get_prefix($R1);
        my $sf   = "$arg_outdir/stats/$name.fq.stats";
        my $stat;
        run_cmd( "seqtk fqchk \Q$R1\E > " . $sf ) unless -e $sf;
        my @row = read_lines($sf);
        $row[2] =~ m/^ALL\s+(\d+)/
          or err ("Can't parse ALL #bases from: $row[2]");
        $stat->{'total_bp'} = $1 * 2;
        return $stat->{'total_bp'};
    }
}

# ================================================================================

=head2 Reference Reads Simulation

If the user does not provide reads for donor and recipient strain,
reads are simulated from the assembled provided genomes.

These simulated reads are needed as negative or positive control for
conversion detection when mapping on donor and recipient strains.

=cut

# ================================================================================

=head3 simulate_reads

Simulate sequencing reads for reference genome when they are not
provided explicitely.

Returns an array containing the path to R1 and R2 compressed using
pigz.

=cut

sub simulate_reads {
    my ( $name, $gnm, $nreads ) = @_;
    my @fq    = map { "$arg_outdir/fq/$name" . $_ } qw(_1.sim.fq _2.sim.fq);
    my $isize = 300;
    my $rsize = floor($arg_rsize);
    run_cmd(
        "wgsim -1$rsize -2$rsize -d$isize -r0 -e0 -N$nreads -R0 -X0 -h $gnm @fq"
    ) unless -e $fq[0] && -e $fq[1];
    return pigz(@fq);
}

=head3 estimate_rsize

Returns the read size estimated from input file using seqtk fqchk.

=cut

# NOTE maybe could be done more easily using samtools faidx or
# something.

sub estimate_rsize {
    my ($R1) = @_;
    my $sf = tempfile();
    run_cmd( "seqtk fqchk \Q$R1\E >" . $sf->filename );
    my $rsize;
    unless ($arg_dryrun) {
        my @row = read_lines( $sf->filename );
        unlink $sf->filename;
        ( $rsize = $row[0] ) =~ s/.*(avg_len: )(.*);.*/$2/
          or err ("Can't estimate read size from fq file");
    }
    else {
        msg("In dry-run mode so estimating read size to 151");
        $rsize = 151;

    }
    return $rsize;
}

=head3 estimate_nreads

Returns the number of reads needed to get to <$arg_coverage>, knowing
that reads are <$arg_rsize> long (specified at the command line or
estimated from sequencing read file 1.)

=cut

sub estimate_nreads {
    my ($gnm)  = @_;
    my $rsize  = $arg_rsize;
    my $gsize  = read_gsize("$gnm.fai");
    my $nreads = ( $arg_coverage * $gsize ) / ( $rsize * 2 );
    return ceil($nreads);
}

=head3 read_gsize

Returns the total size of contigs (or chromosomes) in reference
genomes, by summing all size referenced in the fai file.

=cut

sub read_gsize {
    my ($idx) = @_;
    $idx = abs_path($idx);
    if ($arg_dryrun) {
        msg("In dry-run mode so genome size set to 10 kb");
        return 10000;
    }
    else {
        my @row = read_lines("$idx");
        my @size;
        foreach (@row) {
            my ( $a, $x, $b ) = split /\t/, $_;
            push @size, $x;
        }
        my $gsize = sum(@size);
        return $gsize;
    }
}

=head3 pigz

Compress input file using pigz.

=cut

sub pigz {
    my (@in) = @_;
    my @out = map { $_ . ".gz" } @in;
    foreach (@in) {
        -f $_ . ".gz" or run_cmd("pigz -f --fast -p $arg_cpus $_");
    }
    return @out;
}

=head3 parse_file_list

Returns the list of input reads when <-l> is provided. List has the
form (A1,A2, B1,B2, C1,C2), which is iterated over two by two
downwards, as is done if reads are provided on the command line.

=cut

sub parse_file_list {
    my @lines = read_lines(@_);
    my @out;
    foreach (@lines) {
        chomp;
        my ( $r1, $r2 ) = split /[|\s\t]/, $_;
        push @out, $r1;
        push @out, $r2;
    }
    return @out;
}

=head3 index_genome

Recipient and donor genomes have to be indexed for most downwards
commands to work. This commands make sure that index for reads mapping
(bwa), sequence extracting (samtools faidx) and blastn search
(makeblastdb) exists.

Returns the path to the samtools indexed file.

=cut

sub index_genome {
    my ($gnm) = @_;
    $gnm = abs_path($gnm);
    my @idx = map { $gnm . $_ } qw(.amb .ann .bwt .fai .pac .sa .nhr .nin .nsq);
    if ( notall_exists(@idx) ) {
        dbg("Checking for '@idx'?");
        msg("At least one index file is missing.");
        run_cmd("bwa index $gnm");
        run_cmd("samtools faidx $gnm");
        run_cmd("makeblastdb -dbtype nucl -in $gnm");
    }
    else {
        msg("Genome indexes already exists.");
    }
    return $gnm . ".fai";
}

=head3 get_prefix

Returns the part of first argument name that is before _1. That means
that paired-end reads <should> be named X_{1,2}.[fq|fastq].gz,
otherwise prefixing won't work.

=cut

sub get_prefix {
    my ($x) = @_;
    $x =~ s/(^.*)_1\..*\.gz/$1/;
    return basename($x);
}

=head2 Utilities

=cut

sub newdir { File::Temp->newdir( CLEANUP => !DEBUG ); }
sub tempfile { File::Temp->new( UNLINK => !DEBUG ); }

=head3 read_lines

Return the content of a file as an array.

=cut

sub read_lines {
    my ($fname) = @_;
    open my $FILE, '<', $fname or die("Could not open $fname");
    my @lines = <$FILE>;
    close $FILE;
    return @lines;
}

=head3 tab and comma

Reduce a list to a concatenated string separated by the same
character, either tab or comma.

=cut

sub comma { join ",",  @_; }
sub tab   { join "\t", @_; }

=head3 Caching results

This two commands allow caching costly commands, to skip execution on
next pipeline calling.

  cache($fname, $message)

writes $message to $fname,

  cached($fname)

returns true if $fname exists and echo the message it contains.

=cut

sub cache {
    my ( $fname, $message ) = @_;
    my $cachefile = "$arg_outdir/.cache/$fname";
    msg($message);
    open my $fh, '>', $cachefile;
    print $fh $message;
    close $fh;
}

sub cached {
    my ($fname) = @_;
    my $cachefile = "$arg_outdir/.cache/$fname";
    if ( -f $cachefile ) {
        msg( read_lines($cachefile) );
        return 1;
    }
    else {
        return 0;
    }
}

=head3 notall_exists

Returns true if some elements of input array represents a non-existing
file.

=cut

sub notall_exists {
    return notall { -f } @_;
}

=head3 mkp

Create a path to arguments if script is not in dryrun mode.

=cut

sub mkp { make_path(@_) unless $arg_dryrun; }

=head3 Logging

Logging is intended to be pretty simple.
The interface use msg, err and dbg

=over 4

=item msg

Prints a message to standard error, and append the message to the log
file.

=item err

Prints an error message to standard error using <msg>, and exit script.

=item dbg

Prints an informative message only if DEBUG mode is on. Checks debug
mode is on in global environmental variable DEBUG.

=cut

sub msg {
    my $time = strftime "%H:%M:%S", localtime;
    my $msg  = "# [" . EXE . "] ($time): @_\n";
    log_log($msg);
    print STDERR $msg;
}

sub dbg { DEBUG and say STDERR "# [" . EXE . "] DEBUG: @_"; }

sub err { msg(@_) && exit(1); }

=pod

=item logger, log_cmd and log_log

<Logger> is just a subroutine that appends a text message to a file
Args:
  - msg: a string, said to file rather than printed
  - filename: a string describing a path to the log file

<log_cmd> is intended to write executed bash commands to a file
<log_log> echoes output message to a log file.

=back

=cut

sub logger {
    my ( $msg, $filename ) = @_;
    chomp $msg;
    open my $fh, '>>', $filename;
    say $fh $msg;
    close $fh;
}

sub log_cmd { logger( @_, $log_cmd ); }
sub log_log { logger( @_, $log_cmd ); logger( @_, $log_log ); }

=head3 Running Commands

I run bash command in three flavors:

=over 4

=item run_cmd

  title: run_cmd
  usage: run_cmd($cmd, $toggle, $outcode);
  args:
   - $cmd: the command executed, wrapped in a <bash -c> call
   - $toggle: toggle command echoing in log. True by default
   - $outcode: check that command have error code 0 or $outcode.

=item try_cmd

  title: try cmd
  usage: try_cmd($cmd, $outcode);

Same as run_cmd, also this command explicitely checks that the runned
command returns 0 or $outcode.

=item get_cmd

  title: get_cmd
  usage: get_cmd($cmd)
  args:
   - $cmd: a bash command

Returns the output of running <$cmd> in a bash shell as an in-memory
array that can be iterated over.

=back

=cut

sub io_cmd {
    my $cmd = shift;
    ( my $msg = $cmd ) =~ s/[\n\r\\]//g;
    msg("Running: '$msg'") if $arg_verbose;
    log_cmd($cmd);
    open my $fh, "| $cmd" or err ("Error running command: $!");
    return $fh;
}

sub try_cmd { my ( $cmd, $toggle ) = @_; run_cmd( $cmd, $toggle, 256 ); }

sub force_cmd {
	my ( $cmd ) = @_;
	( my $msg = $cmd ) =~ s/[\n\r\\]//g;
	log_cmd($cmd);
	msg("Running: '$msg'") if $arg_verbose;
	unless ($arg_dryrun) {
		my $script = tempfile();
		open my $fh, '>', $script;
		say $fh $cmd;
		close $fh;
		my $er = system("bash $script");
		$er == 0 or msg("Error $? running command: $!");
	}
}

sub run_cmd {
    my ( $cmd, $toggle, $errcode ) = @_;
    $errcode ||= 0;
    ( my $msg = $cmd ) =~ s/[\n\r\\]//g;
    log_cmd($cmd);
    msg("Running: '$msg'") if $arg_verbose and !$toggle;
    my $er;
    unless ($arg_dryrun) {
        my $script = tempfile();
        open my $fh, '>', $script;
        say $fh $cmd;
        close $fh;
        $er = system("bash $script");
        $er == 0 or $er == $errcode or err ("Error $? running command: $!");
    }
    return $er;
}

sub get_cmd {
    my ($cmd) = @_;
    ( my $msg = $cmd ) =~ s/[\n\r\\]//g;
    log_cmd($cmd);
    msg("Running: '$msg'") if $arg_verbose;
    return `$cmd`;
}

sub echo_info {
    msg("= Parameters: ");
    msg("Script is in verbose mode.") if $arg_verbose;
    msg("Script is in dry-run mode.") if $arg_dryrun;
    msg("Number of allocated threads is '$arg_cpus'.");
    msg("Output directory is '$arg_outdir'.");
    msg("No subsampling will be performed.") if $arg_nosubsample;
    msg("Recipient genome is '$arg_recip'.");
    msg("Donor genome is '$arg_donor'.");
    msg("Target coverage of reference genomes is $arg_coverage");
    $arg_rsize
      ? msg("Read size is $arg_rsize")
      : msg("Will have to estimate read size");
    @arg_recip_reads
      ? msg("Recipient reads are '@arg_recip_reads'")
      : msg("Recipient reads will be simulated");
    @arg_donor_reads
      ? msg("Donor reads are '@arg_donor_reads'")
      : msg("Donor reads will be simulated");
    $arg_file_list
      ? msg("Parsing sample reads from $arg_file_list")
      : msg("Reads file name are parsed from command line.");
    rule();
}

sub greetings {
    msg("Two Way Whole Genome Transformation");
    msg("Written by Samuel Barreto");
    msg("Licensed under GPLv3+");
    rule();
}

sub citations {
    my $citations = <<EOF;
twgt v0.0.1
author: Samuel Barreto
date: 2019-06
place: University of Lyon 1, CNRS, UMR 5558 LBBE

If you use this software, please also cite the following works:

- MindTheGap: integrated detection and assembly of short and long
  insertions. Guillaume Rizk, Anaïs Gouin, Rayan Chikhi and Claire
  Lemaitre. Bioinformatics 2014 30(24):3451-3457.
  http://bioinformatics.oxfordjournals.org/content/30/24/3451
- bwa mem: Li H. and Durbin R. (2009) Fast and accurate short read
  alignment with Burrows-Wheeler Transform. Bioinformatics,
  25:1754-60. [PMID: 19451168]
- samtools, bcftools, htslib: Li H, A statistical framework for SNP
  calling, mutation discovery, association mapping and population
  genetical parameter estimation from sequencing data, Bioinformatics
  (2011) 27(21) 2987-93.

EOF
    say STDERR $citations;
}

sub farewell {
    citations();
    msg("Done.");
}

sub rule {
    my $elapsed_time =
      sprintf( "\t(Elapsed time: %.2f min)", ( time() - $start_time ) / 60 );
    msg(
"------------------------------------------------------------------------"
          . $elapsed_time );
}

sub round { sprintf( "%.2f", @_ ); }

sub usage {
    my ($exitcode) = @_;
    my $EXE        = EXE;
    my $usage      = <<EOF;
Usage: $EXE [options] -- a_{1,2}.fq.gz b_{1,2}.fq.gz ...

Options:

  -i,--reads X_1.fq.gz X_2.fq.gz Sequencing reads of sample
  -l,--list fname.txt            File listing paired end reads
* -r,--recipient rec.fa          Recipient assembled genome
* -d,--donor don.fa              Donor assembled genome

  -D,--donor-reads dR1 dR2       Sequencing reads of donor
  -R,--recipient-reads rR1 rR2   Sequencing reads of recipient
  -s,--read-size 151             Size of sequencing reads
  -C,--coverage 50               Target coverage of reference genomes
  -S,--no-subsample              Disable subsampling to 50x coverage
  -o,--outdir                    Output directory

  -c,--cpus 4                    Number of allocated cpus
  -v,--verbose,--no-verbose      Toggle verbose mode on/off
  -n,--dry-run,--no-dry-run      Toggle dry-run mode on/off
  -h,--help                      Display this message.
EOF

    print STDERR $usage;
    detailed_usage() if $arg_help > 1;
    exit($exitcode);
}

sub detailed_usage {
    print STDERR "Detailed Usage\n";    # FIXME should switch to pod2usage
}

__END__
