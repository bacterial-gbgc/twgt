SHELL=bash

ifndef PREFIX
PREFIX := /usr/local/
endif

.PHONY: install
install:
	ln -s $$PWD/src/twgt.pl $(PREFIX)/bin/twgt

#- TESTS -------------------------------------------------------------

ifdef DRYRUN
RUN :=--dry-run
else
RUN :=
endif


.PHONY: tests try3 try1 try2 cleanex

tests: try3 try1 try2

try3: src/twgt.pl
	src/twgt.pl \
		--outdir example/toy1/twgt \
        --recipient example/toy1/ref.fa \
		--donor example/toy1/dnr.fa \
        --read-size 151 \
		--donor-reads example/toy1/dnr_{1,2}.fq.gz \
        -- example/toy1/{a,b}_{1,2}.fq.gz

try2:
	src/twgt.pl \
		$(RUN) \
		--outdir example/try2 \
		--recipient example/a.fna \
		--donor example/b.fna \
		--donor-reads example/b_{1,2}.fq.gz \
		--recipient-reads example/a_{1,2}.fq.gz \
		--read-size 151 \
		-- example/c_{1,2}.fq.gz

try1:
	src/twgt.pl \
		$(RUN) \
		--recipient example/a.fna \
		--outdir example/try1 \
		--donor example/b.fna \
		--reads example/c_{1,2}.fq.gz \
		--read-size 151 \
		--nocleanup

cleanex:
	rm -rf example/try1
	rm -rf example/try2
	rm -rf example/try3
